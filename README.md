The OpenPGP standard defines the protocols to implement a system that performs cryptographic signing,
encrypting and authentication, e.g. key format, message format etc. are
defined. 
GPG and PGP are software that implement this standard.

RSA is an asymmetric cryptographic algorithm that gpg uses by default.

TailsOS only runs in RAM, meaning it cannot leave any retrievable artifacts
due to the volatile nature of RAM.

entropy was invented in the first place to do with how engines work and to
make them more efficient.
stirling engine (external combustion engine) runs via the difference in
temperature (expansion and contraction of air)
energy is only useful when clumped together. when you use it, it spreads out.
when this happens you can't use it anymore
entropy is a measure of how spread out your energy is. entropy is always
increasing, i.e. it is going from a clumped up state to a spread out state
(e.g. two plates differing in temperature will transfer heat until both
the same). even if we are having a local decrease in entropy, there will be an
external increase in entropy to create this.
eventually all energy in the universe will be spread out (heat death of the
universe, best guess as to how the universe will end. trillions and trillions
of years..), e.g. fossil fuels, the
sun, etc.
lossless no loss of information.

the reason the switch of a barbeque lighter is more difficult than a
traditional button is because we are storing potential energy in a spring
which will then move a hammer that hits a piezoelectric crystal. when you hit
one of these, you generate a voltage across the crystal.
if we compress a piece of quartz (arranged in a particular way) we can shift all the positive charges of the
lattice in one direction and all the negative charges in the other direction.
so, the end points of the crystal are charged (centre is neutral, so can't
draw a lot of current, just a spark)
(orientation and composition of lattice is important to have these effects)
applying a voltage across a piezoelectric crystal will cause it to deform.
in a watch, the quartz crystal will be arranged in the shape of a tuning fork.
the crystal will be shaved down so that its natural resonant frequency is
32,768 kHz (2^15). This is the first power of 2 above 20kHz so humans can't
hear it. It is a power of 2 so that passing it through a chain of flip flops
(after converting the analog sine wave to a square wave for the digital flip
flops)
we can get a single 1 to tick the clock. 
(a flip flop can hold state, i.e. will stay on, i.e. flipped until it is given another single).
the frequency of each flip flop will be half that of the previous, e.g. 2^15,
2^14, etc.
can feed this into a stepper motor which can drive gears that move the clocks
hands, or just directly feed the signal to an lcd display
however, quartz will lose time, so we need something more accurate for GPS
systems. we know that atoms vibrate at precise frequencies are are effectively
immune from environmental factors. so, 1 second is defined as a particular number of
oscillations of a Cs-133 atom. an atomic clock still uses quartz, however a
feedback loop is setup with a Cs atom so when the frequency of the quartz is
dropping, more voltage is applied to keep it the same.

to have a clock you need some oscillation to drive it, e.g. grandfather clock
uses a pendulum that swings every second, electromagnet vibrating a tuning
fork, etc.

WHY COMPUTERS CAN'T COUNT SOMETIMES!
FETCH-EXECUTE CYCLE
every step of the clock, the cpu goes through one of fetch, decode or execute
DOES CPU CLOCK SPEED MATTER (LOSE CLOCK FREQUENCY AS IT PROGRESSES?)

As computers are deterministic, i.e. the same action will be performed in the
same situation, they cannot generate truly random numbers. (VIDEO) chaos key is true hardware independent random
number generator, good for
introducing entropy in key generation so that they can't be guessed
the yubikey performs the encryption and decryption, i.e. the computer never
has direct access to the key as oppose to a generic usb

We must create gpg keys to perform cryptographic operations.
To mitigate the effects of impersonation due to private key loss, it is best
to use an everlasting master key for certification and sub-keys that expire for
for practical operations.
`gpg --expert --full-generate-key`
`gpg --expert --edit-key $KEY_ID` (addkey; save; select
signing/encryption/authentication, 1y expiry)

gpg --list-secret-keys
gpg --detach-sign test.txt
gpg --verify test.txt.sig (signature and file must be in same directory)

SSH is typically used to securely convey commands to a remote server.
asymmetric is used to exchange key for subsequent symmetric operations (this is because symmetric is much faster)
you can use ssh or gpg keys (really just rsa based keys)
authentication is determinable by in the intended recipient only, as oppose to
signatures.

master key is a public/private key pair (off a rsa signature??) that will be
your long term identity. we want to keep this secret
have different keys for encryption and signing. encryption keys may be handed
to others so they can view your encrypted files, e.g. work colleagues.
`gpg --expert --full-generate-key` (only select encrypt and sign capabilities)
what is authenticate option?
[c] certificate means it is used to verify other subkeys which are used for
practical tasks, e.g. encryption, signing.
WE SHOULD ONLY HAVE SUBKEYS ON MACHINE!


ascii armor is the textual representation of binary data. oftentimes it is
used to represent binary encrypted data as ascii
gpg --output master.asc --armor --export-secret-keys %KEY_ID% (USB backup with
veracrypt AES)
gpg --output subkeys.asc --armor --export-secret-subkeys %KEY_ID% (USB backup with
veracrypt AES)
gpg --output pubkey.asc --armor --export %KEY_ID% (USB backup)

gpg --send-key %KEY_ID% (this will propagate amongst a server pool to avoid a
single point of failure) (also upload to keyserver.pgp.com)

.gpg (a binary format) are significantly larger than they have to be due to metadata and
redundancy. so, use paperkey.

gpg --import pubkey.asc or gpg --recv %KEY_ID%
gpg --encrypt file.txt --armor --recipient %KEY_ID% --output file-enc.txt
gpg --decrypt file-enc.txt --armor
gpg --detach-sig file.txt --output file.sig
gpg --verify file.txt file.sig

ciphertext is the plaintext encrypted result (the algorithm to do this is
called a cipher)

note that you are downloading the master public key. there may be subkeys attached
to this that are used behind the scenes by gpg to perform encryption, signing
etc.
fingerprint and keyid are succinct ways to reference a key. keyid is shorter
and more useful for offline reference where collisions are not likely with
your small keyring.
`gpg --list-sigs %KEY_ID%`
addresses of public keys you have imported are listed (gpg --check-sigs
%KEY_ID% will do this)
`gpg --sign-key %KEY_ID%` The public key must be reuploaded to a keyserver.
Ideally you would encrypt the .asc with the users public key they reupload it.

set up git to use gpg key.

we need a public ip (ping to verify), as oppose to a private ip, e.g 10.xxxx (ifconfig)
cloud means we are sharing computing power

local dns in /etc/hosts file. get dns registrar.

ssh-keyscan -t ecdsa <ip> < ssh-keygen -lf 

`passwd` to change root password
for server security we should compartmentalise services, i.e. run each
service from a different system user 
`adduser <username> <group>; su - <username>` ctrl-d exits a shell
`groupadd <group>
`usermod -aG <group> ${USER}`
`deluser <username> <group>
`newgrp <group>`
`cat /etc/passwd; id`


`systemctl status/start/enable` (enable starts it at boot)

/etc/drone/docker-compose.yml:
version: '3'

volumes are where to persist files in containers. /var is where the system
writes to during operation as oppose to /lib which is static
unix sockets allow for IPC
services:
  drone_server:
    container_name: drone
    image: drone/drone:1.9.1
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/drone:/data
    restart: always
    env_file:
      - /etc/drone/server.env 
  drone_runner:
    container_name: drone_runner
    image: drone/drone-runner-docker:1
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    restart: always
    env_file:
      - /etc/drone/runner.env 

/etc/drone/server.env:
DRONE_SERVER_HOST=

/etc/systemd/system/drone.service:
[Unit]
Description=Drone server

[Service]
Restart=Always (restart if something fails)
ExecStart=docker-compose -f /etc/drone/docker-compose.yml up
ExecStop=docker-compose -f /etc/drone/docker-compose.yml stop

[Install]
WantedBy=multi-user.target
(multi-user.target is a system state whereby network services and logins are
available, but a local GUI is not started, i.e. graphical.target. 
So, we are establishing a dependency to only enable when we are in this state)

curl -L is important links will often follow to some verbose aws link! /usr/local/bin is for self-compiled binaries, as oppose
to package managed in /usr/bin

register http://<ip> as OAuth url and http://<ip>/login as callback
install drone server docker image on vps
navigate to vps ip. this will show all of our repositories. choose a
repository with .yaml to create a webhook.

we want to install docker engine (docker-ce) and docker compose (for
multi-container applications)
docker runner will be running at all times just like the server. we can see
with `docker ps`. check if runner connected to server with `docker logs
<container_id>`

as containers utilise the host OS resources, cannot run windows container on
linux and vice versa
Can do non-host testing, i.e. run the tests but on a different platform and/or
compiler. We can still check the business logic and collect code metrics. Just
not platform specific things.

CURIOUS ABOUT THE WORKFLOW OF A EMBEDDED PROJECT. HOW MUCH IS DESIGNING ON A
PCB, USING PRE-BUILT MICROCONTROLLER ETC.

It allows us to always have a working build to show and makes releases as fast
as possible to get feedback from users to ensure we are working on introducing
lots of changes quickly rather than all together at a later date.
The CI server gives you an official pass/fail check, no more "it builds on my
machine!"
Can also monitor build sizes and durations and other code metrics.
Deployment using scp to a server?? Perhaps to github releases? itch.io? beta
builds for early access clients? upload for beta testers to use

CI is best utilised with git branching, see git flow and agile (merging onto
master should automatically be sent to production. in the merge request we
should see the relevant testing stages that the current branch should have
passed) 
the git flow branches are to establish swim lanes, i.e. distinguish job
sharing

to start work, we look at TODO issues on JIRA. Assign self to it and move into
DOING. Best to fork repo on Github as this will create a connection between
repos (as we can't push directly to origin without being a member)
once have pushed feature branch, we need to initiate a pull request to have it
merged onto develop (have develop as base). name pull request with jira issue
id.
`git fetch` will update remotes, i.e. bring down all branches on origin (no
files will be changed). `git checkout <branch-name>`
to do any work, we create a feature branch off the develop branch (named
according to JIRA issues).
to merge from feature to develop, there is protection with code reviews and
pull requests
UAT (user acceptance testing) is performed on staging/release branch (named
according to semver) before master.
DOES SOMETHING ON THE MASTER BRANCH EQUATE TO THE END OF A SPRINT. DO WE
PERFORM UAT ETC.? DON'T THINK SO.
have ci run on develop and master branch.
STAGING IS DONE ON A WORKSTATION IDENTICAL TO THAT OF PRODUCTION.
  --> FUZZING HERE (advanced topic.... gamozalabs)?
  --> INPUT REPLAYS? (prog.exe -input file.rec)
  --> HANDMADE HERO TESTING METHODOLOGIES!
  --> prototype/alpha/beta stages? playtesting?
  --> HOW DOES THIS LOOK FOR EMBEDDED SYSTEMS?
to enforce this, create branch protection rules on master, develop and staging
branches.
ownership of tasks is clear

IT IS ESSENTIAL YOU UNDERSTAND THE BUSINESS REQUIREMENTS AND PROJECT HISTORY
define clear business vision and how the project addresses this -> roadmap

JIRA:
create project -> create issue (ticket). try to have each task take no longer
than 3 days. once experienced, we would want this to be 1 day

stories: planned work for a specific feature
  --> as a user, i need the ability to delete an element in the list.
  BE SPECIFIC
tasks:
  --> create new database for ai characters 
bugs:
  --> the player's health is overflowing
epics:
  --> containers of stories and tasks. define these at beginning of scrum
  planning session. once prioritised epics, fill out the stories that complete
  them (this is done by dragging in backlog window)
story points are indicators of task complexity. use fibonacci numbers as they
map to estimation errors. observe how many story points you can get done in
each sprint, e.g. 15. 2-3 hours = 1pt. 3 days = 13 points

these issues are put in scrum backlog which are handled in sprint planning
and subsequent 2 week sprint. while a sprint is active, no new work should be
added

once a sprint is started from the backlog, we go to active sprints window
(board).
perhaps introduce a blocked column?
a person should be working on a single project at a time

1. formulate product backlog. this is meeting with customers to formulate a
list of everything they want (at the time being). will typically phrase these
as user stories.
2. sprint planning takes things out of backlog. will have an end-user focused
goal, e.g. at the end of the sprint users can update their shopping carts
3. sprint. daily scrum is asking what did you do yesterday, what are you going
to do today and do you have any impediments (answer these questions on a
dedicated channel in discord?).(will refine product backlog
during this)
4. sprint retrospective is what should we try, what should we stop, what
should we continue to do?
WHEN NOT WORKING ON A PROJECT FULL-TIME THE LENGTH OF A SPRINT IS NOT FIXED
LOOK A CODING GARDEN TRELLO BOARD. WHEN YOU WANT TO DO SOMETHING, LOOK AT
IT IN THE TRELLO BOARD AND ASK A QUESTION ON DISCORD ABOUT STARTING IT.
DAILY SCRUM WILL BE INITIATED IN 'STAND UP' CHANNEL WHEN YOU ARE STARTING WORK
ON A NEW FEATURE. THE QUESTIONS ARE THE SAME, HOWEVER THE TIMEFRAME MAY BE
WEEKS OR MONTHS IN THE PAST, NOT A DAY.
--> BACKLOG | TODO | DOING (THINGS ASSIGNED) | MERGED/DEPLOYED | USER TESTING
| DONE


working with customers:
  --> progress report based on git commits? send to
  slack/discord/email-newsletter/documentation website?
user requirements into code
technology feasibility tests
  --> how much is it going to cost and how long is it going to take?
  --> vision -> components to achieve vision -> properties of components ->
  how would this be assembled?
scrum/agile process
multidisciplinary teams
full software lifecycle:
  --> agile is rapid iterative delivery with the ability to change to
  different requirements/plans quickly. a lot of buzz words. we want to create
  business value. we are creating things with the user in mind. feedback cycle
  is good as the world is quickly changing so we want to know if what we are
  doing is heading in the right direction. also what the customer actually
  wants may change as the project progresses. short iterations allow us to
  integrate customer requests
  --> kanban agile is best for bug fixes (no backlog)
  --> scrum best for regular feature updates
projects that demonstrate how you overcame common problems in embedded systems
when can we add more things to the backlog?

HiL (hardware in a loop) testing for embedded systems?

when to use FPGAs (hardware languages to modify circuitry) over microcontroller (sofware)

.docker.yaml
---
kind: pipeline
type: docker
name: rws

# NOTE(Ryan): To test different architectures, introduce multiple
              pipelines $
# dictionary
platform:
  arch: amd64

# the docker images are cached
# drone automatically creates a shared volume between steps
# array
steps:
- name: rws-test
  image: gcc
  when:
    branch:
    - master
    event:
    - tag
  commands:
  - git clone https://github.com/google/googletest.git googletest \
    && mkdir googletest/build \
    && cd /googletest/build \
    && cmake .. && cmake --build . --target install --config Release -- -j
    $(getconf _n_processors_onln) \
    && cd / && rm -rf /googletest
  - bash ubuntu-build.bash
  - ./test --gtest_output="xml:/home/something/folder" 
  - bash <(curl -s https://codecov.io/bash) -t <token> -f
    /home/something/folder/output.xml
- name: rws-deploy
  image: plugins/github-release
  settings:
  .....

drone ci status badges
integrate sourcegraph extension for codecov in github?

`doxygen -g;` logo?
INPUT = ../src/ ../include/
FILE_PATTERNS = *.c *.h
EXTRACT_ALL = YES (show for code that hasn't been commented yet)
LATEX = NO
PROJECT_NAME = "Name"
SHOW_FILES = NO
DISABLE_INDEX = YES (disable top bar)
GENERATE_TREEVIEW = YES (enable side bar)
'html/index.html'
doxy comment identifiers
/*!
 * \author Ryan McClue
 * \copyright spx license identifier 
 * \warning some message here
 *
 * \bug some bug here
 *
 * \mainpage Something here
 * \section section_name Section
 * content of the section
 * \subsection .....
 */
//!< comment to go after a variable
/*!
 * \brief description of a function
 * \param param_name param description
 * \details some details here
 */

best practices to storing receipts? use accounting software?? ask parents how
tax returns work. what is the deal with tax file declaration? how is tax
payed.
